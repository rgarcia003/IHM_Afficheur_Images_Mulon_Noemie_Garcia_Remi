﻿using ExifLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projet_Afficheur_Images
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
          
            InitializeComponent();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + @"\ImagesTest";
            DataContext = new DataModel(path);
           
        }

    


        private void Diaporama_Click(object sender, RoutedEventArgs e)
        {
            List<JPGFileInfo> liste = ((DataModel)DataContext).Fichiers;

            Diapo Diaporama = new Diapo(liste);
            
            Diaporama.Show();
        }

        private void EditItem_Click(object sender, RoutedEventArgs e)
        {
          
           
        }
   

    }
}
