﻿using ExifLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Projet_Afficheur_Images
{
    /// <summary>
    /// Logique d'interaction pour Diapo.xaml
    /// </summary>
    public partial class Diapo : Window
    {
       
        int nbImages;
        int courrant;
        List<JPGFileInfo> fichiers;
        DispatcherTimer timer;
        public Diapo(List<JPGFileInfo> Context)
        {
            InitializeComponent();
            courrant = 0;
            fichiers = Context;
            image.DataContext = Context[courrant];
                
            nbImages = Context.Count();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 3);
            timer.Tick += timer_Tick;
            timer.Start();
            this.PreviewKeyDown += new KeyEventHandler(Controls);
        }

        void timer_Tick(object sender, EventArgs e)
        {

            image.DataContext = fichiers[courrant++ % nbImages];
 
        }

       private void Controls(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
            if (e.Key == Key.Right)
                courrant = (++courrant) % nbImages;
            if (e.Key == Key.Left) {
                courrant--;
                if (courrant < 0)
                    courrant = nbImages - 1;
            }

            if (e.Key == Key.Space)
            {
                if (!timer.IsEnabled)
                    timer.Start();
                else
                    timer.Stop();
            }

            image.DataContext = fichiers[courrant];
               
        }
    }
}
